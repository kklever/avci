﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.TypedDataSets;

using TimeLine.BusinessObjects;

namespace TimeLine.Modules.Client.ProdBACustom
{
    public class busBabCustom : busBab
    {
		public new dsBabCustom tSet { get; set; }
		
		//KMK 19-03-11

        public busBabCustom()
        {
			tSet = new dsBabCustom(dSet);
        }

        public override int Retrieve()
        {
            return base.Retrieve();
        }

        public override int Save()
        {
            return base.Save();
        }
    }
}
