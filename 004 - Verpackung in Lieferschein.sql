﻿IF NOT EXISTS (select * from "sys"."syscolumns" where tname='belpos' and cname='avci_verpackid') THEN 
ALTER TABLE "belpos" ADD "avci_verpackid" nchar(20) NULL   ; 
INSERT INTO "dba".pbcatcol (pbc_tnam, pbc_ownr, pbc_cnam, pbc_labl, pbc_cmnt)
VALUES ('belpos', 'dba', 'avci_verpackid', 'VerpackungsID', NULL); 
END IF;
IF NOT EXISTS (select * from "tlsys_db_column" where tblname='belpos' and colname='avci_verpackid') THEN 
INSERT INTO tlsys_db_column (tblname, colname, caption, kommentar, col_typ, col_width, col_decimals, col_index, default_val, allownull_jn, pkey_jn, control_type, control_def) 
VALUES('belpos', 'avci_verpackid', 'VerpackungsID', NULL, 'nchar', 20, 1, 149, NULL, 1, 0, 'xLookupGrid', 'lg_verpackg'); 
END IF;
IF EXISTS (select * from "sys"."syscolumns" WHERE tname='belpos' and cname='avci_verpackid') THEN 
ALTER TABLE "belpos" DROP "avci_verpackid"  ; 
END IF;
DELETE FROM "dba".pbcatcol  
WHERE pbc_tnam='belpos' AND pbc_cnam='avci_verpackid' ;
DELETE FROM tlsys_db_column 
WHERE tblname = 'belpos' and colname='avci_verpackid' ;
DELETE FROM tlsys_db_join 
WHERE (tbl1 = 'belpos' AND CHARINDEX('avci_verpackid IS', columns) >= 1) OR (tbl2 ='belpos' AND CHARINDEX(' IS avci_verpackid', columns) >= 1) ;
IF EXISTS (select * from "sys"."syscolumns" WHERE tname='belpos' and cname='avci_verpackid') THEN 
ALTER TABLE "belpos" DROP "avci_verpackid"  ; 
END IF;
DELETE FROM "dba".pbcatcol  
WHERE pbc_tnam='belpos' AND pbc_cnam='avci_verpackid' ;
DELETE FROM tlsys_db_column 
WHERE tblname = 'belpos' and colname='avci_verpackid' ;
DELETE FROM tlsys_db_join 
WHERE (tbl1 = 'belpos' AND CHARINDEX('avci_verpackid IS', columns) >= 1) OR (tbl2 ='belpos' AND CHARINDEX(' IS avci_verpackid', columns) >= 1) ;
IF NOT EXISTS (select * from "sys"."syscolumns" where tname='bel' and cname='avci_verpackungsid') THEN 
ALTER TABLE "bel" ADD "avci_verpackungsid" nchar(20) NULL   ; 
INSERT INTO "dba".pbcatcol (pbc_tnam, pbc_ownr, pbc_cnam, pbc_labl, pbc_cmnt)
VALUES ('bel', 'dba', 'avci_verpackungsid', 'VerpackungsID', NULL); 
END IF;
IF NOT EXISTS (select * from "tlsys_db_column" where tblname='bel' and colname='avci_verpackungsid') THEN 
INSERT INTO tlsys_db_column (tblname, colname, caption, kommentar, col_typ, col_width, col_decimals, col_index, default_val, allownull_jn, pkey_jn, control_type, control_def) 
VALUES('bel', 'avci_verpackungsid', 'VerpackungsID', NULL, 'nchar', 20, 0, 182, NULL, 1, 0, 'xLookupGrid', 'lg_verpackg'); 
END IF;
