﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.TypedDataSets;

using TimeLine.BusinessObjects;

namespace TimeLine.Modules.Client.BelegUebernCustom
{
    public class busBelUebernCustom : busBelUebern
    {
		public new dsBeluebernCustom tSet { get; set; }

		public busBelUebernCustom()
		{
			//tSet = new dsBeluebernCustom(dSet);
		}
		
		public busBelUebernCustom(string userId)
		{
			//tSet = new dsBeluebernCustom(dSet);
		}
		
		public override int StartUebernahme(int zielBelTyp, int specifySerialCharge, bool speichern)
		{
			var rc = base.StartUebernahme(zielBelTyp, specifySerialCharge, speichern);
			
			if( zielBelTyp == Enums.BelTyp.Auftragsbestätigung || zielBelTyp == Enums.BelTyp.Lieferschein )
			{
				if( zielBusDoc.Header.pers_typ.ToInt(0) == Enums.PersTyp.Lieferant ) 
				{
					zielBusDoc.RetrievePers(1, zielBusDoc.Header.pers_nr.ToInt(0), true);
					zielBusDoc.Save();
				}
			}
			
			return rc;
		}
    }
}
