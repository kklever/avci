﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.Client.Framework.NonVisual.RoutedEvents;
using TimeLine.Client.Controls;
using TimeLine.Client.Framework.Controls;

using TimeLine.Client.Controls.DX.Docking;
using TimeLine.Client.Controls.DX.Grid;

using TimeLine.Client.Controls.DX.Chart;
using TimeLine.Client.Controls.DX.Gauge;
using TimeLine.Client.Controls.DX.Pivot;

using TimeLine.Client.Controls.TX.RTF;

using TimeLine.Modules.Belege;
using TimeLine.TypedDataSets;

namespace TimeLine.Modules.Client.BelegUebernCustom
{
    public partial class wndBelegUebernCustom : wndBelegUebern
    {
		public new busBelUebernCustom BusObj
		{
			get
			{
				return base.BusObj as busBelUebernCustom;
			}
		}
		
		public override void StartUebern()
		{
			/*xMessageBox.Show(cbQuellBelTyp.Value.ToString() + "|" + cbZielBelTyp.Value.ToString());
			if(cbQuellBelTyp.Value.ToInt() == Enums.BelTyp.WareneingangLieferschein.ToInt() && (cbZielBelTyp.Value.ToInt() == Enums.BelTyp.Auftragsbestätigung.ToInt() || cbZielBelTyp.Value.ToInt() == Enums.BelTyp.Lieferschein.ToInt()))
			{
                //xMessageBox.Show(BusObj.quellBusDoc.tSet.bel.FirstOrDefault().pers_typ.ToStringNN());
                BusObj.zielBusDoc.tSet.bel.FirstOrDefault().pers_typ = Enums.PersTyp.Kunde;
			}*/
			
			base.StartUebern();
		}

		#region override WHO methods
		
		#endregion
    }
}
