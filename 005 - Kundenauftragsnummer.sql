﻿IF NOT EXISTS (select * from "sys"."syscolumns" where tname='bel' and cname='avci_kundenAufNr') THEN 
ALTER TABLE "bel" ADD "avci_kundenAufNr" nchar(120) NULL   ; 
INSERT INTO "dba".pbcatcol (pbc_tnam, pbc_ownr, pbc_cnam, pbc_labl, pbc_cmnt)
VALUES ('bel', 'dba', 'avci_kundenAufNr', 'avci_kundenAufNr', 'Kundenauftragsnummer'); 
END IF;
IF NOT EXISTS (select * from "tlsys_db_column" where tblname='bel' and colname='avci_kundenAufNr') THEN 
INSERT INTO tlsys_db_column (tblname, colname, caption, kommentar, col_typ, col_width, col_decimals, col_index, default_val, allownull_jn, pkey_jn, control_type, control_def) 
VALUES('bel', 'avci_kundenAufNr', 'avci_kundenAufNr', 'Kundenauftragsnummer', 'nchar', 120, 1, 183, NULL, 1, 0, 'xTextBox', NULL); 
END IF;
