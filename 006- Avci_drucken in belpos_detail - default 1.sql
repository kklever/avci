﻿IF NOT EXISTS (select * from "sys"."syscolumns" where tname='belpos_detail' and cname='avci_drucken') THEN 
ALTER TABLE "belpos_detail" ADD "avci_drucken" integer NULL  DEFAULT 0 ; 
INSERT INTO "dba".pbcatcol (pbc_tnam, pbc_ownr, pbc_cnam, pbc_labl, pbc_cmnt)
VALUES ('belpos_detail', 'dba', 'avci_drucken', 'avci_drucken', NULL); 
END IF;
IF NOT EXISTS (select * from "tlsys_db_column" where tblname='belpos_detail' and colname='avci_drucken') THEN 
INSERT INTO tlsys_db_column (tblname, colname, caption, kommentar, col_typ, col_width, col_decimals, col_index, default_val, allownull_jn, pkey_jn, control_type, control_def) 
VALUES('belpos_detail', 'avci_drucken', 'avci_drucken', NULL, 'integer', 4, 0, 43, '0', 1, 0, 'xTextBox', NULL); 
END IF;
IF NOT EXISTS (select * from "sys"."syscolumns" where tname='belpos_detail' and cname='avci_drucken') THEN 
ALTER TABLE "belpos_detail" ADD "avci_drucken" integer NULL  DEFAULT 1 ; 
INSERT INTO "dba".pbcatcol (pbc_tnam, pbc_ownr, pbc_cnam, pbc_labl, pbc_cmnt)
VALUES ('belpos_detail', 'dba', 'avci_drucken', 'avci_drucken', NULL); 
END IF;
IF NOT EXISTS (select * from "tlsys_db_column" where tblname='belpos_detail' and colname='avci_drucken') THEN 
INSERT INTO tlsys_db_column (tblname, colname, caption, kommentar, col_typ, col_width, col_decimals, col_index, default_val, allownull_jn, pkey_jn, control_type, control_def) 
VALUES('belpos_detail', 'avci_drucken', 'avci_drucken', NULL, 'integer', 4, 0, 43, '1', 1, 0, 'xTextBox', NULL); 
END IF;
