﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.TypedDataSets;

using TimeLine.Modules.Produktion;

namespace TimeLine.Modules.Client.RMInfoCustom
{
    public class busRMInfoCustom : busRMInfo
    {
		public new dsRmInfoCustom tSet { get; set; }

        public busRMInfoCustom()
        {
			tSet = new dsRmInfoCustom(dSet);
        }

        public override int Retrieve()
        {
            return base.Retrieve();
        }

        public override int Save()
        {
            return base.Save();
        }
    }
}
