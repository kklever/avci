﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.Client.Framework.NonVisual.RoutedEvents;
using TimeLine.Client.Controls;
using TimeLine.Client.Framework.Controls;

using TimeLine.Client.Controls.DX.Docking;
using TimeLine.Client.Controls.DX.Grid;

using TimeLine.Client.Controls.DX.Chart;
using TimeLine.Client.Controls.DX.Gauge;
using TimeLine.Client.Controls.DX.Pivot;

using TimeLine.Client.Controls.TX.RTF;

using TimeLine.Modules.MDE;

namespace TimeLine.Modules.Client.MDEEC
{
    public partial class wndMDEECCustom : wndMDEEC
    {
        //System.Windows.Controls.Control focusedElement;
		TimeLine.Client.Controls.xTextBox focusedElement;
		
		#region override WHO methods

		public override void ButtonClicked(xButton sourceControl)
		{
			switch (sourceControl.Name)
			{
			case "btnLogin":
				string tbMitarbContent = tbMitarb.Value.ToStringNN();
				string tbMitarbRef = Sql.TrySelectValue("SELECT mitarb.name FROM mitarb WHERE mitarb.karten_nr = '{0}'",tbMitarbContent).ToStringNN();
				string tbMitarbKuerzel = Sql.TrySelectValue("SELECT mitarb.kuerzel FROM mitarb WHERE mitarb.karten_nr = '{0}'",tbMitarbContent).ToStringNN();
				tbMitarb.Value = tbMitarbKuerzel;
				dp.Header = "Kein Benutzer gefunden";
				if(tbMitarbRef != string.Empty || tbMitarbRef != "")
				{
					dp.Header = tbMitarbRef;
				}
				break;
			case "btnLoginAfo":
				xSearch 
				break;
			case "b_0":
				tbMitarb.Value += "0";
				break;
			case "b_1":
				tbMitarb.Value += "1";
				break;
			case "b_2":
				tbMitarb.Value += "2";
				break;
			case "b_3":
				tbMitarb.Value += "3";
				break;
			case "b_4":
				tbMitarb.Value += "4";
				break;
			case "b_5":
				tbMitarb.Value += "5";
				break;
			case "b_6":
				tbMitarb.Value += "6";
				break;
			case "b_7":
				tbMitarb.Value += "7";
				break;
			case "b_8":
				tbMitarb.Value += "8";
				break;
			case "b_9":
				tbMitarb.Value += "9";
				break;
			case "b_punkt":
				tbMitarb.Value += ".";
				break;
			case "b_komma":
				tbMitarb.Value += ",";
				break;
			case "b_del":
				tbMitarb.Value = string.Empty;
				break;
			case "b_back":
				tbMitarb.Value = (tbMitarb.Value.ToString() != "" || tbMitarb.Value.ToString() != string.Empty ? tbMitarb.Value.ToString().Substring(0,tbMitarb.Value.ToString().Length-1):tbMitarb.Value);
				break;
			case "b_0_a":
				tbsBaAfo.Value += "0";
				break;
			case "b_1_a":
				tbsBaAfo.Value += "1";
				break;
			case "b_2_a":
				tbsBaAfo.Value += "2";
				break;
			case "b_3_a":
				tbsBaAfo.Value += "3";
				break;
			case "b_4_a":
				tbsBaAfo.Value += "4";
				break;
			case "b_5_a":
				tbsBaAfo.Value += "5";
				break;
			case "b_6_a":
				tbsBaAfo.Value += "6";
				break;
			case "b_7_a":
				tbsBaAfo.Value += "7";
				break;
			case "b_8_a":
				tbsBaAfo.Value += "8";
				break;
			case "b_9_a":
				tbsBaAfo.Value += "9";
				break;
			case "b_punkt_a":
				tbsBaAfo.Value += ".";
				break;
			case "b_komma_a":
				tbsBaAfo.Value += ",";
				break;
			case "b_del_a":
				tbsBaAfo.Value = string.Empty;
				break;
			case "b_back_a":
				tbsBaAfo.Value = (tbsBaAfo.Value.ToString() != "" || tbsBaAfo.Value.ToString() != string.Empty ? tbsBaAfo.Value.ToString().Substring(0,tbsBaAfo.Value.ToString().Length-1):tbsBaAfo.Value);
				break;
			case "b_0_ab":
				focusedElement.Value += "0";
				break;
            case "b_1_ab":
                focusedElement.Value += "1";
                break;
            case "b_2_ab":
                focusedElement.Value += "2";
                break;
            case "b_3_ab":
                focusedElement.Value += "3";
                break;
            case "b_4_ab":
                focusedElement.Value += "4";
                break;
            case "b_5_ab":
                focusedElement.Value += "5";
                break;
            case "b_6_ab":
                focusedElement.Value += "6";
                break;
            case "b_7_ab":
                focusedElement.Value += "7";
                break;
            case "b_8_ab":
                focusedElement.Value += "8";
                break;
            case "b_9_ab":
                focusedElement.Value += "9";
                break;
            case "b_punkt_ab":
                focusedElement.Value += ".";
                break;
            case "b_komma_ab":
                focusedElement.Value += ",";
                break;
            case "b_del_ab":
                focusedElement.Value = string.Empty;
                break;
            case "b_back_ab":
                focusedElement.Value = (focusedElement.Value.ToString() != "" || focusedElement.Value.ToString() != string.Empty ? focusedElement.Value.ToString().Substring(0,focusedElement.Value.ToString().Length-1):focusedElement.Value);
                break;
			default:
				break;
			}
			
			base.ButtonClicked(sourceControl);
		}
		
		public override void Opened()
		{
			base.Opened();
			tbMitarb.Value = string.Empty;
		}
		
		public override void ItemFocusChanged(Control sourceControl)
		{
			base.ItemFocusChanged(sourceControl);
			
			focusedElement = tbMenge;
			
			switch (sourceControl.Name)
			{
			case "tbMenge":
				focusedElement = tbMenge;
				break;
			case "tbAusschuss":
				focusedElement = tbAusschuss;
				break;
			default:
				break;
			}
		}
		
		
		#endregion
    }
}
