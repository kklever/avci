﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.Client.Framework.NonVisual.RoutedEvents;
using TimeLine.Client.Controls;
using TimeLine.Client.Framework.Controls;

using TimeLine.Client.Controls.DX.Docking;
using TimeLine.Client.Controls.DX.Grid;

using TimeLine.Client.Controls.DX.Chart;
using TimeLine.Client.Controls.DX.Gauge;
using TimeLine.Client.Controls.DX.Pivot;

using TimeLine.Client.Controls.TX.RTF;

using TimeLine.Modules.Info;

namespace TimeLine.Modules.Client.InfoOpenOrdersCustom
{
    public partial class wndInfoOpenOrdersCustom : WndInfoOpenOrders
    {
		public new BusInfoOpenOrdersCustom BusObj
		{
			get
			{
				return base.BusObj as BusInfoOpenOrdersCustom;
			}
		}

		#region override WHO methods

		/// <summary>
        /// Place to do module's and controls' initializations. 
		/// At this point, all controls are created.
		/// </summary>
        public override void Opened()
        {
            base.Opened();
        }

		public override void ItemChanged(UIElement container, FrameworkElement element, object selectedItem, object newValue, object oldValue)
        {
            base.ItemChanged(container, element, selectedItem, newValue, oldValue);
            var rows = xdgOpenOrders.SelectedRows;
	        switch (element.Name)
	        {
	        	case "tbMengeUebern":
				if (xrbgBuySale.Value.ToInt() == 6)
				{
					foreach (var row in rows)
					{
						Sql.Execute("UPDATE belpos SET uebernommen = {0} WHERE bel_nr = {1} AND bel_typ = {2} AND posnr = {3}",
						           row["uebernommen"].ToInt(),row["bel_nr"].ToInt(),row["bel_typ"].ToInt(),row["posnr"].ToInt());
					}
				}
	        		break;
				case "xrbgBuySale":
				if (xrbgBuySale.Value.ToInt() == 6)
				{
					tbMengeUebern.IsReadOnly = false;
				}
				else
				{
				    tbMengeUebern.IsReadOnly = true;
				}
				break;
	        	default:
	        		break;
	        }
        }
		#endregion
    }
}
