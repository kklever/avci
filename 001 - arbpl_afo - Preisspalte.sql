﻿IF NOT EXISTS (select * from "sys"."syscolumns" where tname='arbpl_afo' and cname='avci_afopreis') THEN 
ALTER TABLE "arbpl_afo" ADD "avci_afopreis" double NULL  DEFAULT 0 ; 
INSERT INTO "dba".pbcatcol (pbc_tnam, pbc_ownr, pbc_cnam, pbc_labl, pbc_cmnt)
VALUES ('arbpl_afo', 'dba', 'avci_afopreis', 'AFO-Preis/Stück', 'Preis pro Stück'); 
END IF;
IF NOT EXISTS (select * from "tlsys_db_column" where tblname='arbpl_afo' and colname='avci_afopreis') THEN 
INSERT INTO tlsys_db_column (tblname, colname, caption, kommentar, col_typ, col_width, col_decimals, col_index, default_val, allownull_jn, pkey_jn, control_type, control_def) 
VALUES('arbpl_afo', 'avci_afopreis', 'AFO-Preis/Stück', 'Preis pro Stück', 'double', 8, 0, 40, '0', 1, 0, 'xTextBox', NULL); 
END IF;
