using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.TypedDataSets;

using TimeLine.Modules.Info;

namespace TimeLine.Modules.Client.InfoArtCustom
{
    public class BusInfoArtCustom : BusInfoArt
    {
		public new dsInfoArtCustom tSet { get; set; }

        public BusInfoArtCustom()
        {
			tSet = new dsInfoArtCustom(dSet);
        }

        public override int Retrieve()
        {
            return base.Retrieve();
        }

        public override int Save()
        {
            return base.Save();
        }
    }
}
