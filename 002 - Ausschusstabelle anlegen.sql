﻿IF NOT EXISTS (SELECT 1 FROM "SYS"."SYSTABLE" WHERE table_name ='avci_ausschussgründe') THEN 
 CREATE TABLE "avci_ausschussgründe" (
"lfdnr" integer NOT NULL  , 
"bez" nchar(40) NULL  , 
"text" nchar(10000) NULL  ) ;
GRANT ALL ON "avci_ausschussgründe" TO DBA; 
INSERT INTO "dba".pbcattbl (pbt_tnam, pbt_ownr, pbd_fhgt, pbd_fwgt, pbd_fitl, pbd_funl, pbd_fchr, pbd_fptc, pbd_ffce, pbh_fhgt, pbh_fwgt, pbh_fitl, pbh_funl, pbh_fchr, pbh_fptc, pbh_ffce, pbl_fhgt, pbl_fwgt, pbl_fitl, pbl_funl, pbl_fchr, pbl_fptc, pbl_ffce, pbt_cmnt)
 VALUES ('avci_ausschussgründe', 'dba', -9, 400, 'N', 'N', 0, 34, 'Tahoma', -9, 400, 'N', 'N', 0, 34, 'Tahoma', -8, 400 , 'N', 'N', 0, 34, 'Tahoma', NULL) ; 
INSERT INTO "dba".pbcatcol (pbc_tnam, pbc_ownr, pbc_cnam, pbc_labl, pbc_cmnt)
 VALUES ('avci_ausschussgründe', 'dba', 'lfdnr', 'Nummer', NULL); 
INSERT INTO "dba".pbcatcol (pbc_tnam, pbc_ownr, pbc_cnam, pbc_labl, pbc_cmnt)
 VALUES ('avci_ausschussgründe', 'dba', 'bez', 'Bezeichnung', NULL); 
INSERT INTO "dba".pbcatcol (pbc_tnam, pbc_ownr, pbc_cnam, pbc_labl, pbc_cmnt)
 VALUES ('avci_ausschussgründe', 'dba', 'text', 'Text', NULL); 
 
END IF;
IF NOT EXISTS (SELECT 1 FROM "tlsys_db_table" WHERE tblname ='avci_ausschussgründe') THEN 
 INSERT INTO tlsys_db_table(tblname, caption, kommentar) VALUES ('avci_ausschussgründe', NULL, NULL) ; 
 
END IF;
IF NOT EXISTS (select * from "tlsys_db_column" where tblname='avci_ausschussgründe' and colname='lfdnr') THEN 
INSERT INTO tlsys_db_column (tblname, colname, caption, kommentar, col_typ, col_width, col_decimals, col_index, default_val, allownull_jn, pkey_jn, control_type, control_def) 
VALUES('avci_ausschussgründe', 'lfdnr', 'Nummer', NULL, 'integer', 4, 0, 0, NULL, 0, 1, 'xTextBox', NULL); 
END IF;
IF NOT EXISTS (select * from "tlsys_db_column" where tblname='avci_ausschussgründe' and colname='bez') THEN 
INSERT INTO tlsys_db_column (tblname, colname, caption, kommentar, col_typ, col_width, col_decimals, col_index, default_val, allownull_jn, pkey_jn, control_type, control_def) 
VALUES('avci_ausschussgründe', 'bez', 'Bezeichnung', NULL, 'nchar', 40, 1, 1, NULL, 1, 0, 'xTextBox', NULL); 
END IF;
IF NOT EXISTS (select * from "tlsys_db_column" where tblname='avci_ausschussgründe' and colname='text') THEN 
INSERT INTO tlsys_db_column (tblname, colname, caption, kommentar, col_typ, col_width, col_decimals, col_index, default_val, allownull_jn, pkey_jn, control_type, control_def) 
VALUES('avci_ausschussgründe', 'text', 'Text', NULL, 'nchar', 10000, 1, 2, NULL, 1, 0, 'xTextBox', NULL); 
END IF;
IF NOT EXISTS (SELECT 1 FROM SYSIDX WHERE index_name = 'avci_ausschussgründe' AND index_category = 1) THEN 
 ALTER TABLE "avci_ausschussgründe" ADD PRIMARY KEY ("lfdnr")  
 END IF;
INSERT INTO tlsys_db_index(table_name, index_name, category, index_unique) VALUES ('avci_ausschussgründe', 'avci_ausschussgründe', 1, 1);
INSERT INTO tlsys_db_index_col(table_name, index_name, col_name, sequence, ordering) VALUES ('avci_ausschussgründe', 'avci_ausschussgründe', 'lfdnr', 0, 'A');
