﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.Client.Framework.NonVisual.RoutedEvents;
using TimeLine.Client.Controls;
using TimeLine.Client.Framework.Controls;

using TimeLine.Client.Controls.DX.Docking;
using TimeLine.Client.Controls.DX.Grid;

using TimeLine.Client.Controls.DX.Chart;
using TimeLine.Client.Controls.DX.Gauge;
using TimeLine.Client.Controls.DX.Pivot;

using TimeLine.Client.Controls.TX.RTF;
using TimeLine.Modules.Item;
using TimeLine.TypedDataSets;

namespace TimeLine.Modules.Client.ItemCustom
{
    public partial class wndItemCustom : wndItem
    {
		public new busArtCustom BusObj
		{
			get
			{
				return base.BusObj as busArtCustom;
			}
		}
		
		
		
		#region new WHO methods
		
		public void InsertZSL()
		{
			var rows = xdgArbPl.VisibleRows;
			BusObj.tSet.zsl_persart.DeleteAll();
			//Insert new ZSLRow
			foreach (var row in rows)
			{
				if(row["arbgkat_nr"].ToStringNN() == null || row["arbgkat_nr"].ToStringNN() == "")break;
				if(row["arbgkat_nr"].ToString() == Sql.TrySelectValue("SELECT id FROM zsl WHERE id ='{0}'",row["arbgkat_nr"].ToStringNN()).ToStringNN())
				{
					try
					{
						var newzslrow = BusObj.tSet.zsl_persart.CreateRow();
						newzslrow.berech_art = Enums.BerechArt.AbsolutProMengeneinheit;
						newzslrow.arbgkat_id = Sql.TrySelectValue("SELECT id FROM arbgkat WHERE id ='{0}'",row["arbgkat_nr"]).ToStringNN();
						newzslrow.art_bez1 = tbbez1.Value.ToStringNN();
						newzslrow.art_bez2 = tbbez2.Value.ToStringNN();
						newzslrow.art_suchwort= tbsuchwort.Value.ToStringNN();
						newzslrow.artnr = tbsartnr.Value.ToStringNN();
						newzslrow.typ = 10;
						newzslrow.lfdnr = Sql.TrySelectValue("SELECT MAX(lfdnr) FROM zsl_persart").ToInt(0) + 1;
						newzslrow.zslid = row["arbgkat_nr"].ToStringNN();
						newzslrow.bez1 = row["bez1"].ToStringNN();
						newzslrow.wert = row["avci_afopreis"].ToDoubleNN();
					}
					catch (Exception e)
					{
						xMessageBox.Show(e.ToStringNN(), "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);
						throw;
					}
				}
			}
		}
		#endregion
		
		#region override WHO methods
		
		public override int OnSave()
		{
			if (xcbProd.Value.ToStringNN() == "1")
			{
				InsertZSL();
			}
			return base.OnSave();
		}
		#endregion
    }
}
