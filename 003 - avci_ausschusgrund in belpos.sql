﻿IF NOT EXISTS (select * from "sys"."syscolumns" where tname='belpos' and cname='avci_ausschuss') THEN 
ALTER TABLE "belpos" ADD "avci_ausschuss" integer NULL   ; 
INSERT INTO "dba".pbcatcol (pbc_tnam, pbc_ownr, pbc_cnam, pbc_labl, pbc_cmnt)
VALUES ('belpos', 'dba', 'avci_ausschuss', 'Ausschussgrund', 'Ist verknüpft mit der Kundenspezifischenavci_ausschussgründe Tabelle'); 
END IF;
IF NOT EXISTS (select * from "tlsys_db_column" where tblname='belpos' and colname='avci_ausschuss') THEN 
INSERT INTO tlsys_db_column (tblname, colname, caption, kommentar, col_typ, col_width, col_decimals, col_index, default_val, allownull_jn, pkey_jn, control_type, control_def) 
VALUES('belpos', 'avci_ausschuss', 'Ausschussgrund', 'Ist verknüpft mit der Kundenspezifischenavci_ausschussgründe Tabelle', 'integer', 4, 0, 148, NULL, 1, 0, 'xTextBox', NULL); 
END IF;

UPDATE tlsys_db_column SET tblname = 'belpos', colname = 'avci_ausschuss', caption = 'Ausschussgrund', kommentar = 'Ist verknüpft mit der Kundenspezifischenavci_ausschussgründe Tabelle' , col_typ = 'integer', col_width = 4, col_decimals = 0, col_index = 148 , default_val = NULL, allownull_jn = 1, pkey_jn = 0, control_type = 'xLookupGrid', control_def = 'avci_ausschussgrund'
WHERE tblname = 'belpos' AND colname= 'avci_ausschuss' ;