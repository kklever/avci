﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.TypedDataSets;

using TimeLine.BusinessObjects;

namespace TimeLine.Modules.Client.Belege
{
    public class busBelRightsCustom : busBelRights
    {

		public busBelRightsCustom()
		{
			// KMK 2019-03-22
			// Aus einem WE-Lieferschein soll ein Lieferschein erstellt werden können
			this.Belege[Enums.BelTyp.WareneingangLieferschein].Transitions.Add(Enums.BelTyp.Lieferschein);
			//WEL -> VAB
			this.Belege[Enums.BelTyp.WareneingangLieferschein].Transitions.Add(Enums.BelTyp.Auftragsbestätigung);
		}
    }
}
