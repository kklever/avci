﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.Client.Framework.NonVisual.RoutedEvents;
using TimeLine.Client.Controls;
using TimeLine.Client.Framework.Controls;

using TimeLine.Client.Controls.DX.Docking;
using TimeLine.Client.Controls.DX.Grid;

using TimeLine.Client.Controls.DX.Chart;
using TimeLine.Client.Controls.DX.Gauge;
using TimeLine.Client.Controls.DX.Pivot;

using TimeLine.Client.Controls.TX.RTF;

using TimeLine.Modules.Belege;
using TimeLine.TypedDataSets;
using TimeLine.BusinessObjects.Utils;

namespace TimeLine.Modules.Client.Belege
{
    public partial class wndBelegCustom : wndBeleg
    {
		
		#region override WHO methods
		public override void ItemChanged(UIElement container, FrameworkElement element, object selectedItem, object newValue, object oldValue)
		{
			base.ItemChanged(container, element, selectedItem, newValue, oldValue);
			if(element.Name == tbavci_drucken.Name)
			{
				xdgZSL.SelectedRow["summ_jn"] = newValue.ToInt();
				BusObj.CalculateBelposPrice(xdgBelpos.SelectedRow, true, false);
			}
            if(BelTyp == 2 || BelTyp == 3)
            {
                if (BusObj.tSet.Args_typ == 2)
                {
                    BusObj.tSet.Args_typ = 1;
                }
            }
		}
		
		
        protected override void CreateBeleg(int typ, int nr)
        {
            base.CreateBeleg(typ, nr);
            if(BelTyp == 2 || typ == 3)
            {
                if(typ == 3)
                {
                    //var tdsAdr = this.ServerName != null ? new dsAdr(this.ServerName, this.sessionId) : new dsAdr();
                    //xMessageBox.Show("AdressLoad aus CreateBeleg");
                    //  Prepare the session objects for Address Load response window...
                    //clientGlobals.Session["BusDoc"] = this;
                    //clientGlobals.Session["BusCRM"] = tdsCrm;
                    //clientGlobals.Session["BusAdr"] = tdsAdr;
					
                    //  Open the response window...
                    //xWHOModule rspAddressLoad = ModuleManager.Instance.LoadModule("rspAddressLoad");
                    //ModuleManager.Instance.OpenModule(rspAddressLoad, OpenModuleStyle.Response, l.Translate("Adress-Wizard"));
                }
                if (BusObj.tSet.Args_typ == 2)
                {
                    BusObj.tSet.Args_typ = 1;
                }
            }
        }
		
		
        protected override void OnRetrieve(Object[] args)
        {
            base.OnRetrieve(args);
			if(BelTyp == 2 || BelTyp == 3)
			{
				if (BusObj.tSet.Args_typ == 2)
				{
					BusObj.tSet.Args_typ = 1;
				}
			}
			if(BusObj.tSet.Args_typ == 4 || BusObj.tSet.Args_typ == 3)
			{
				var belpos = xdgBelpos.VisibleRows;
				foreach (var pos in belpos)
				{
					var rows = xdgZSL.VisibleRows;
					foreach (var row in rows)
					{
						if(row["avci_drucken"].ToString() == "1")
						{
							row["summ_jn"] = 1;
						}
						else
						{
							row["summ_jn"] = 0;
						}
					}
				}
			}
        }
		
		public override void PostOpen()
		{
			base.PostOpen();
			if(BusObj.tSet.Args_typ == 4 || BusObj.tSet.Args_typ == 3)
			{
				var belpos = xdgBelpos.VisibleRows;
				foreach (var pos in belpos)
				{
					var rows = xdgZSL.VisibleRows;
					foreach (var row in rows)
					{
						if(row["avci_drucken"].ToString() == "1")
						{
							row["summ_jn"] = 1;
						}
						else
						{
							row["summ_jn"] = 0;
						}
					}
				}
			}
		}
		
        public override int OnPreSave()
        {
            if(BelTyp == 2 || BelTyp == 3)
            {
                if (BusObj.tSet.Args_typ == 2)
                {
                    BusObj.tSet.Args_typ = 1;
                }
            }
            return base.OnPreSave();
        }
		
        //int gesamt_gewicht = 0;
		
        //public override int OnSave()
        //{
        //    tbgewicht_gesamt.Value = gesamt_gewicht;
        //    return base.OnSave();
        //}
		
        //public override int OnPreSave()
        //{
        //    gesamt_gewicht = tbgewicht_gesamt.Value.ToInt(0);
        //    return base.OnPreSave();
        //}
		#endregion
    }
}
